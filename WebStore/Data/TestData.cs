﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebStore.Models;

namespace WebStore.Data
{
    public static class TestData
    {
        public static List<Employee> Employees { get; } = new()
        {
            new Employee { Id = 1, FirstName = "Петр", LastName = "Петров", Patronymic = "Петрович", Age = 23, Birthday = DateTime.Parse("01/01/1992") },
            new Employee { Id = 2, FirstName = "Виктор", LastName = "Пронькин", Patronymic = "Павлович", Age = 30, Birthday = DateTime.Parse("20/08/1999") },
            new Employee { Id = 3, FirstName = "Иван", LastName = "Куликов", Patronymic = "Дмитреевич", Age = 27, Birthday = DateTime.Parse("15/07/1997") },
            new Employee { Id = 4, FirstName = "Макар", LastName = "Захаров", Patronymic = "Фридманович", Age = 32, Birthday = DateTime.Parse("01/09/1982") }
        };
    }
}
